package com.soltec.smetrics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmetricsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmetricsApplication.class, args);
    }

}
