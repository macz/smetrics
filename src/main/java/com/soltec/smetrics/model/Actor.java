package com.soltec.smetrics.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.net.URL;

@Entity
@Table(name = "business_actors",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"username"})
        })
public class Actor extends BaseEntity {
    @Size(min = 1, max = 100)
    @Column(name = "username")
    private String username;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "first_name")
    private String firstName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "photo")
    private URL photo;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    @Size(min = 4, max = 30)
    private String phone;

    @Column(name = "role")
    @Size(min = 1, max = 100)
    private String role;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "business_unit_id")
    private BusinessUnit businessUnit;
}
