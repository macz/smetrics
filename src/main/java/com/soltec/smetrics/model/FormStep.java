package com.soltec.smetrics.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "input_form_steps")
public class FormStep extends BaseEntity {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "title")
    private String title;

    @Column(name = "tooltip")
    private String tooltip;

    @Column(name = "\"order\"")
    private Integer order;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "input_form_id")
    private Form form;
}
