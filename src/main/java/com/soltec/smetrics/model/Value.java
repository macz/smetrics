package com.soltec.smetrics.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(name = "input_values")
public class Value extends BaseEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "input_form_id")
    private Form form;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "input_key_id")
    private InputKey inputKey;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "\"key\"")
    private String key;

    @Size(min = 1, max = 200)
    @Column(name = "value")
    private String value;

    @Column(name = "decimal_value")
    private BigDecimal decimalValue;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "business_actor_id")
    private Actor actor;
}
