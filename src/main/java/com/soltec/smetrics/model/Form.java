package com.soltec.smetrics.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "input_forms")
public class Form extends BaseEntity {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "instructions")
    private String instructions;

    @Column(name = "icon")
    @Size(min = 1, max = 50)
    private String icon;

    @Column(name = "steps")
    @Basic(optional = false)
    @NotNull
    private Integer steps = 1;

    @Column(name = "favorite")
    private boolean favorite = false;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "business_unit_id")
    private BusinessUnit businessUnit;
}
