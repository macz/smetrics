package com.soltec.smetrics.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(name = "input_keys",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"name"})
        })
public class InputKey extends BaseEntity {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "tooltip")
    private String tooltip;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "type")
    private String type;

    @Column(name = "measurable")
    private boolean measurable = false;

    @Column(name = "min")
    private BigDecimal min;

    @Column(name = "max")
    private BigDecimal max;

    @Column(name = "\"precision\"")
    private long precision;

    @Column(name = "mask")
    private String mask;

    @Column(name = "default_value")
    private String defaultValue;

    @Column(name = "\"required\"")
    private boolean required = false;

    @Column(name = "validation_expression")
    private String validationExpression;

    @Column(name = "hidden")
    private boolean hidden = false;
}
