package com.soltec.smetrics.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "metrics")
public class Metric extends BaseEntity {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String description;

    @Column(name = "\"precision\"")
    private Integer precision;

    @Column(name = "target_value")
    @Basic(optional = false)
    @NotNull
    private BigDecimal targetValue;

    @Column(name = "warning_value")
    private BigDecimal warningValue;

    @Column(name = "critical_value")
    private BigDecimal criticalValue;

    @Column(name = "favorite")
    @Basic(optional = false)
    @NotNull
    private boolean favorite = false;

    @Column(name = "last_value")
    @Basic(optional = false)
    @NotNull
    private BigDecimal lastValue = BigDecimal.ZERO;

    @Column(name = "last_measurement_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastMeasurementTime;

    @Column(name = "expiration_date")
    @Temporal(TemporalType.DATE)
    private Date expirationDate;

    @Column(name = "target_date")
    @Temporal(TemporalType.DATE)
    private Date targetDate;

    @Column(name = "calculation_cron")
    private String calculationCron;

    @Column(name = "calculation_script")
    private String calculationScript;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "business_unit_id")
    private BusinessUnit businessUnit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "metric_category_id")
    private MetricCategory metricCategory;
}
