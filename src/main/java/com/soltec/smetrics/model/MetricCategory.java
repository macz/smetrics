package com.soltec.smetrics.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "metric_categories",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"name"})
        })
public class MetricCategory extends BaseEntity {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
}
