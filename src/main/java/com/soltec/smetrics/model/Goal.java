package com.soltec.smetrics.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "goals")
public class Goal extends BaseEntity {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "title")
    private String title;

    @Column(name = "resume")
    private String resume;

    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "target_date")
    private Date targetDate;

    @Size(min = 1, max = 20)
    @Column(name = "status")
    private String status = STATUS_DRAFT;

    @Basic(optional = false)
    @NotNull
    @Column(name = "progress")
    private BigDecimal progress = BigDecimal.ZERO;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "business_unit_id")
    private BusinessUnit businessUnit;
}
