package com.soltec.smetrics.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Base Entity Contains the basic registry properties for any project entity.
 * <p>
 * This class implements the soft delete pattern throw deletedAt field.
 *
 * @author macolinares
 * @since 0.0.1-SNAPSHOT
 */
@MappedSuperclass
@SQLDelete(sql = "UPDATE deletedAt SET deletedAt=current_timestamp() WHERE id=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deletedAt is null")
@ToString(exclude = "id")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String STATUS_DRAFT = "draft";
    public static final String STATUS_ARCHIVED = "archived";
    public static final String STATUS_CLOSED = "closed";
    public static final String STATUS_PAID = "paid";
    public static final String STATUS_APPROVED = "approved";
    public static final String STATUS_CANCELED = "canceled";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(32)")
    private String uuid;

    @Basic(optional = false)
    @NotNull
    @Column(name = "archived")
    private short archived;

    @Column(name = "created_at", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "updated_at")
    private Date updatedAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deleted_at")
    private Date deletedAt;

    @Version
    private Integer version;

    @PreRemove
    public void delete() {
        this.deletedAt = Calendar.getInstance().getTime();
    }
}
