package com.soltec.smetrics.model;

import javax.persistence.*;

@Entity
@Table(name = "input_form_key_step_orders")
public class FormKeyStepOrder extends BaseEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "input_form_id")
    private Form form;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "input_key_id")
    private InputKey inputKey;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "input_form_step_id")
    private FormStep formStep;

    @Column(name = "\"order\"")
    private Integer order;
}
